# PoC Kyverno

## Requirements

[kind](https://kind.sigs.k8s.io/)

## Installation

Install kyverno using [helm chart (as usual for our team) in standalone mode](https://kyverno.io/docs/installation/methods/#standalone). 

/!\ For production we'll need HA, but that's note the scope of this PoC. /!\

## Getting started

Follow the steps detailed in [docs](https://kyverno.io/docs/introduction/#quick-start-guides) in order to apply your first Validating Policy.

### Validate Policy

Deploy the following cluster policy, which requires Pods having a label named `team` in their metadata:

```
kubectl apply -f manifests/kyverno_policies/validate_require_labels.yaml
```

Create a Pod without the label, kyverno will stop the action through its ValidatingWebhook because the Pod does not have the required label:

```
kubectl run nginx --image nginx poc-kyverno=true
```

Try it again with the label:

```
kubectl run nginx --image nginx --labels poc-kyverno=true,team=infra
```


#### Policy Report

Now that we have a Pod and a Cluster Policy, we can get a report of current Policy tests:

```
kubectl get policyreport -w
```

We could perhaps scrape that with prometheus in order to alert us if we have some reports with errors.

#### Clean

```
kubectl delete all -l poc-kyverno=true
kubectl delete clusterpolicies -l poc-kyverno=true
```

### Mutate Policy

Deploy the following cluster policy, which adds `team` label with value `bravo` in Pod's metadata:

```
kubectl apply -f manifests/kyverno_policies/mutate_add_label.yaml
```

Create a Pod without the label, kyverno will add the label through its MutatingWebhook:

```
kubectl run nginx --image nginx --labels poc-kyverno=true
```

Check that Pod has "team" label, but label was not overriden because it already existed:

```
kubectl get pods --show-labels
```

Update the policy to replace the label name, team, by squad, and apply the policy:

```
kubectl apply -f manifests/kyverno_policies/mutate_add_label.yaml
```

Check that Pod DOES NOT have "squad" label:

```
kubectl get pods --show-labels
```

#### Mutate Existing

In order to [mutate existing resources](https://kyverno.io/docs/writing-policies/mutate/#mutate-existing-resources), kyverno does it in brackground, through background controller.

For that, we'll need to setup RBAC permissions.

Create a namespace:

```
kubectl create namespace foo
```

Create a pod in that namespace:

```
kubectl run nginx --image nginx --labels poc-kyverno=true -n foo
```

Check that Pod has no "team" label:

```
kubectl get pods --show-labels
```

Create a policy to add label `team=bravo` to existing pods in namespace `foo`:

```
kubectl apply -f manifests/kyverno_policies/mutate_existing_add_label.yaml
```

Arrg, I forgot to configure RBAC permissions to allow kyverno update *targeted* resources:

```
kubectl apply -f manifests/rbac_kyverno_update_pods_all_clusterrole.yaml
```

Retry to apply mutate policy to update existing resources:

```
kubectl apply -f manifests/kyverno_policies/mutate_existing_add_label.yaml
```

Check that Pod has "team" label, with `bravo` value:

```
kubectl get pods --show-labels
```

#### Clean

```
kubectl delete clusterrolebindings -l poc-kyverno=true
kubectl delete clusterrole -l poc-kyverno=true
kubectl delete clusterpolicies -l poc-kyverno=true
kubectl delete namespace foo
```

### Generate Policy

We will use a Kyverno generate policy to generate an image pull secret in a new Namespace.

Create the secret to replicate:

```
kubectl -n default create secret docker-registry regcred \
  --docker-server=myinternalreg.corp.com \
  --docker-username=john.doe \
  --docker-password=Passw0rd123! \
  --docker-email=john.doe@corp.com
```

Apply the policy to sync the secret to synchonise to other namespaces:

```
kubectl apply -f manifests/kyverno_policies/generate_sync_secret.yaml
```

Create a namespace:

```
kubectl create namespace foo
```

Check that the secret is there:

```
kubectl get secrets -n foo
```

We could use that feature to sync common network policies also :)

Create a NetworkPolicy in default namespace:

```
kubectl apply -f manifests/network_policy.yaml
```

Create a Generate policy to copy the NetworkPolicy to other namespaces:

```
kubectl apply -f manifests/kyverno_policies/generate_sync_netpol.yaml
```

Check that the network policy was **not created** in foo namespace. Why? If we want the generate policy to [work with existing resources](https://kyverno.io/docs/writing-policies/generate/#generate-for-existing-resources), we need to explicitly define a flag `generateExisting: true` in the spec.

Add the flag `generateExisting: true` in the spec and apply again:

```
kubectl apply -f manifests/kyverno_policies/generate_sync_netpol.yaml
```

Check that the network policy was created in foo namespace.

#### Clean

```
kubectl delete all -l poc-kyverno=true -n foo
kubectl delete -f manifests/network_policy.yaml
kubectl delete clusterpolicies -l poc-kyverno=true
kubectl delete namespace foo
```


## Other policy tests

### Validate that cpu limits are not set

Apply the following validate policy:

```
kubectl apply -f manifests/kyverno_policies/validate_forbid_cpu_limits.yaml
```

Try to create a pod which has cpu limits defined in it's container specs:

```
kubectl apply -f manifests/pod_with_limits.yaml
```
